package org.test.jvm;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.jar.JarOutputStream;
import java.util.zip.ZipEntry;

public class SimplifyRtJarService {

	public static final void cutRtJar(String applicationJarPath, String outputRtJarPath, String comand) {
		try {
			Set<String> applicationClasses = JarUtil.getClasses(applicationJarPath);
			//java -verbose -jar C:\Users\Administrator\git\GreenJVMMake\source\GreenJVMMake_GUI\fps_test.jar
			Process verboseProcess = Util.exec(comand);
			BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(verboseProcess.getInputStream()));
			ArrayList<String> usingClasses = new ArrayList<String>();
			while (true) {
				try {
					verboseProcess.exitValue();
					break;
				} catch (IllegalThreadStateException localIllegalThreadStateException) {
				}
				String line = localBufferedReader.readLine();
				if ((line != null) && (line.matches("\\[Loaded .* from .*"))) {
					String className = line.substring(8, line.indexOf(" from "));
					if (!applicationClasses.contains(className)) {
						Util.println("inclued : " + className);
						usingClasses.add(className);
					}
				}
			}
			writeClassesIntoJar(usingClasses, outputRtJarPath);
		} catch (IOException localIOException) {
			localIOException.printStackTrace();
		}
	}

	public static final ClassLoader getClassLoader() {
		ClassLoader classloader;
		if ((classloader = Thread.currentThread().getContextClassLoader()) == null)
			classloader = org.test.jvm.SimplifyRtJarService.class.getClassLoader();
		if (classloader == null)
			classloader = ClassLoader.getSystemClassLoader();
		return classloader;
	}

	public static final void writeClassesIntoJar(List<String> classes, String outputJarPath) throws IOException {
		if ((classes == null) || (classes.size() == 0)) {
			return;
		}
		
		File localFile;
		if (!(localFile = new File(outputJarPath)).exists()) {
			localFile.createNewFile();
		}
		byte[] arrayOfByte = new byte[8192];
		System.out.println(outputJarPath);
		JarOutputStream localJarOutputStream = new JarOutputStream(new FileOutputStream(outputJarPath));
		Iterator<String> localIterator = classes.iterator();
		while (localIterator.hasNext()) {
			String className = localIterator.next();
			Util.println(("includeing ：" + className + ".class").intern());
			className = className.replace('.', '/') + ".class";
			try {
				InputStream localInputStream = getClassLoader().getResourceAsStream(className);
				localJarOutputStream.putNextEntry(new ZipEntry(className));
				int i;
				while ((i = localInputStream.read(arrayOfByte)) >= 0) {
					localJarOutputStream.write(arrayOfByte, 0, i);
				}
				localInputStream.close();
			} catch (Exception localException) {
			}
		}
		localJarOutputStream.putNextEntry(new ZipEntry("META-INF/MANIFEST.MF"));
		localJarOutputStream.write(JarUtil.buildMetaInfo().getBytes());
		localJarOutputStream.finish();
		localJarOutputStream.close();
	}
}
