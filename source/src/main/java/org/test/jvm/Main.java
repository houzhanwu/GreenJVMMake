package org.test.jvm;

import java.io.File;

public class Main {
	
	public static void main(String[] args) {
		args = new String[] { "-t", "da", "-i", "C:\\Users\\Administrator\\git\\GreenJVMMake\\source\\GreenJVMMake_GUI\\fps_test.jar", "-o", "./" };
		String cmdJavaVerbose = "java -verbose";
		String simplifyType = null;
		String applicationJarPath = null;
		String outputJarFolderPath = null;
		if (args.length == 0) {
			Util.print("Simplify the type [da( Desktop Application ) or ca( Console Application )] : \n");
			simplifyType = Util.readLine();
			Util.print("My application file : \n");
			applicationJarPath = Util.readLine();
			Util.print("Output jar folder : \n");
			outputJarFolderPath = Util.readLine();
		} else {
			if (args.length < 6) {
				throw new RuntimeException("The wrong parameters !");
			}
			for (int i = 0; i < args.length; i++) {
				if (i % 2 == 1) {
					if (i == 1) {
						simplifyType = args[i];
					} else if (i == 3) {
						applicationJarPath = args[i];
					} else if (i == 5) {
						outputJarFolderPath = args[i];
					}
				}
			}
		}
		
		boolean isConsoleApplication = "ca".equalsIgnoreCase(simplifyType) || "Console Application".equalsIgnoreCase(simplifyType);
		boolean isDesktopApplication = "da".equalsIgnoreCase(simplifyType) || "Desktop Application".equalsIgnoreCase(simplifyType);
		String comand = cmdJavaVerbose ;
		if(isConsoleApplication) {
			comand += " -cp ";
		} else if(isDesktopApplication) {
			comand += " -jar ";
		} else {
			comand += " ";
		}
		comand += applicationJarPath;
		
		String outputRtJarPath;
		if ((outputRtJarPath = new File(outputJarFolderPath).getAbsolutePath()).endsWith(".")) {
			outputRtJarPath = outputRtJarPath.substring(0, outputRtJarPath.length() - 1);
		}
		SimplifyRtJarService.cutRtJar(applicationJarPath, outputRtJarPath + "rt.jar", comand);
	}
}
