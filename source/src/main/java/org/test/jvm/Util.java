package org.test.jvm;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public final class Util {
	
	private static final BufferedReader SystemInBufferedReader = new BufferedReader(new InputStreamReader(new BufferedInputStream(System.in)));

	public static void println(Object paramObject) {
		System.out.println(paramObject);
	}

	public static void print(Object paramObject) {
		System.out.print(paramObject);
	}

	private static String _readLine() {
		try {
			return SystemInBufferedReader.readLine();
		} catch (IOException localIOException) {
		}
		return null;
	}

	public static String readLine() {
		return _readLine();
	}

	public static Process exec(String paramString) throws IOException {
		return Runtime.getRuntime().exec(paramString);
	}
}
